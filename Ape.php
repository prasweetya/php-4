<?php
require_once("Animal.php");

$sheep = new Animal("shaun");

class Ape extends Animal {
    public $legs = 4;
    public function yell() {
        echo "Auooo";
    }
}
?>